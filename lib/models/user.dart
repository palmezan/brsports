import 'package:cloud_firestore/cloud_firestore.dart';

class User {
  User({
    this.id,
    this.name,
    this.email,
    this.password,
    this.backHand,
    this.city,
    this.imgPic,
    this.matchesPlace,
    this.strongHand,
    this.nickName,
    this.points,
    this.phone,
    //this.rankings
    //this.rankings
  });

  User.fromDocument(DocumentSnapshot document) {
    id = document.documentID;
    name = document.data['name'] as String;
    email = document.data['email'] as String;
    city = document.data['city'] as String;
    backHand = document.data['backHand'] as String;
    //rankings = List<String>.from(document.data['rankings'] as List<dynamic>);
    imgPic = document.data['imgPic'] as String;
    matchesPlace = document.data['matchesPlace'] as String;
    strongHand = document.data['strongHand'] as String;
    nickName = document.data['nickName'] as String;
    points = document.data['points'] as int;
    phone = document.data['phone'] as String;
  }

  String id;
  String name;
  String email;
  String password;
  String city;
  //List<String> rankings;
  String backHand;
  String imgPic;
  String matchesPlace;
  String strongHand;
  String nickName;
  int points;
  String phone;

  String confirmPassword;

  DocumentReference get firestoreRef =>
      Firestore.instance.document('users/$id');

  Future<void> saveData() async {
    await firestoreRef.setData(toMap());
  }

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'email': email,
    };
  }
}
