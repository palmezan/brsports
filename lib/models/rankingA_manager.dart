import 'package:appfinalbrsports/models/player.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';

class RankingAManager extends ChangeNotifier {
  RankingAManager() {
    _loadAllPlayers();
  }
  final Firestore firestore = Firestore.instance;
  final points = 0;

  List<Player> allPlayers = [];

  Future<void> _loadAllPlayers() async {
    firestore.collection('users').orderBy('points', descending: true)
    .snapshots().listen((snapshot) {
      allPlayers.clear();
      for (final DocumentSnapshot document in snapshot.documents) {
        allPlayers.add(Player.fromDocument(document));
      }
      notifyListeners();
    });
  }
}
