import 'package:cloud_firestore/cloud_firestore.dart';

class Player {

Player({
    this.id,
    this.name,
    this.email,
    this.backHand,
    this.city,
    this.imgPic,
    this.matchesPlace,
    this.strongHand,
    this.nickName,
    this.points,
    this.phone,
    this.userId,
    this.position
});


  Player.fromDocument(DocumentSnapshot document) {
    id = document.documentID;
    name = document['name'] as String;
    email = document['email'] as String;
    nickName = document['nickName'] as String;
    city = document['city'] as String;
    backHand = document['backHand'] as String;
    strongHand = document['strongHand'] as String;
    matchesPlace = document['matchesPlace'] as String;
    points = document['points'] as int;
    phone = document['phone'] as String;
    imgPic = document['imgPic'] as String;
    userId = document['uid'] as String;
  }


  int position;
  String name;
  String email;
  String nickName;
  String city;
  String imgPic;
  String matchesPlace;
  String backHand;
  String phone;
  String strongHand;
  int points;
  String userId;
  String id;
}
