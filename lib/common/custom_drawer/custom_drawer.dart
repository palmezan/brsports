import 'package:appfinalbrsports/common/custom_drawer/custom_drawer_header.dart';
import 'package:appfinalbrsports/common/custom_drawer/drawer_tile.dart';
import 'package:flutter/material.dart';

class CustomDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Stack(
        children: [
          Container(
           color: Color(0xFF6BDA62),
          ),
          ListView(
            children: <Widget>[
              CustomDrawerHeader(),
                SizedBox(
                  height: 20,
                ),
              DrawerTile(iconData: Icons.home, title: 'Início', page: 0),
              DrawerTile(
                  iconData: Icons.sports_tennis_rounded,
                  title: 'Desafios',
                  page: 1),
              DrawerTile(iconData: Icons.person, title: 'Meu perfil', page: 2),
              DrawerTile(
                  iconData: Icons.notifications, title: 'Notificações', page: 3),
              DrawerTile(iconData: Icons.settings, title: 'Configurações', page: 4),
              DrawerTile(iconData: Icons.info, title: 'Sobre o app', page: 5),
            ],
          ),
        ],
      ),
    );
  }
}
