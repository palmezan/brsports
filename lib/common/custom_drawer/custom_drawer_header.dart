import 'package:appfinalbrsports/models/user_manager.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CustomDrawerHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: 300,
        child: Column(
          children: [
            Expanded(
              child: Center(
                child: Image.asset(
                  "assets/images/logo.png",
                  height: 280,
                ),
              ),
            ),
            Consumer<UserManager>(
              builder: (_, userManager, __) {
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      userManager.isLoggedIn
                          ? Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                shape: BoxShape.circle,
                                boxShadow: [
                                  BoxShadow(
                                      blurRadius: 20,
                                      color: Colors.black38,
                                      spreadRadius: 1),
                                ],
                              ),
                              child: SizedBox(
                                height: 70,
                                width: 70,
                                child: CircleAvatar(
                                  backgroundImage:
                                      NetworkImage(userManager.user.imgPic),
                                ),
                              ),
                            )
                          : Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                shape: BoxShape.circle,
                                boxShadow: [
                                  BoxShadow(
                                      blurRadius: 20,
                                      color: Colors.black38,
                                      spreadRadius: 1),
                                ],
                              ),
                              child: SizedBox(
                                height: 70,
                                width: 70,
                                child: CircleAvatar(
                                  backgroundColor: Colors.grey.shade200,
                                ),
                              ),
                            ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text(
                            userManager.user?.name ?? '',
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          // Text(
                          //   userManager.isLoggedIn ? 'Rankings: ' + userManager.user.rankings.first + ' | ' + userManager.user.rankings.last
                          //   : '',
                          //   style: TextStyle(
                          //     color: Colors.white,
                          //     fontSize: 16,
                          //     fontWeight: FontWeight.w400,
                          //   ),
                          // ),
                          GestureDetector(
                            onTap: () {
                              if (userManager.isLoggedIn) {
                                userManager.signOut();
                                Navigator.of(context)
                                    .pushReplacementNamed('/login');
                              } else {
                                Navigator.of(context).pushNamed('/login');
                              }
                            },
                            child: Text(
                              userManager.isLoggedIn ? 'Sair' : 'Faça o login',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
