import 'package:appfinalbrsports/models/player.dart';
import 'package:appfinalbrsports/models/rankingA_manager.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PlayerListTile extends StatelessWidget {
  PlayerListTile(this.player);

  final Player player;

  @override
  Widget build(BuildContext context) {
    final hasImage = player.imgPic != null;

    return Consumer<RankingAManager>(builder: (_, rankingAManager, __) {
      final position = rankingAManager.allPlayers.indexOf(player) + 1;

      return Column(
        children: [
          Container(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 16),
              child: InkWell(
                onTap: () {
                  Navigator.of(context)
                      .pushNamed('/playera', arguments: player);
                },
                child: Stack(
                  children: <Widget>[
                    Positioned.fill(
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Stack(
                          children: [
                            Card(
                              elevation: 10,
                              margin: EdgeInsets.only(left: 50),
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Container(
                                height: 65,
                              ),
                            ),
                            Positioned.fill(
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Card(
                                  elevation: 10,
                                  margin: EdgeInsets.only(left: 50),
                                  color: Colors.white,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: Container(
                                    height: 70,
                                    width: 75,
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          Text(
                                            '#' + position.toString(),
                                            style: TextStyle(
                                                color: Color(0xFF117D08),
                                                fontWeight: FontWeight.bold,
                                                fontSize: 17),
                                          ),
                                          Text(
                                            player.points.toString() + ' pts',
                                            style: TextStyle(
                                                color: Colors.grey.shade500,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 14),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            boxShadow: [
                              BoxShadow(
                                  blurRadius: 20,
                                  color: Colors.black38,
                                  spreadRadius: 1),
                            ],
                          ),
                          child: SizedBox(
                            height: 70,
                            width: 70,
                            child: CircleAvatar(
                              backgroundColor: Colors.grey.shade400,
                              backgroundImage: hasImage
                                  ? NetworkImage(player.imgPic)
                                  : ExactAssetImage('assets/images/avatar.png'),
                            ),
                          ),
                        ),
                        Padding(
                            padding: const EdgeInsets.only(
                              left: 15,
                            ),
                            child: Container(
                              width: 210,
                              child: Text(
                                player.name.toString(),
                                softWrap: true,
                                style: TextStyle(
                                    color: Color(0xFF117D08),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16),
                              ),
                            )),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      );
    });
  }
}
