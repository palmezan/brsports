import 'package:appfinalbrsports/common/custom_drawer/custom_drawer.dart';
import 'package:appfinalbrsports/models/rankingA_manager.dart';
import 'package:appfinalbrsports/screens/rankingA/components/playersA_list_tile.dart';
import 'package:appfinalbrsports/tiles/header_simples_tile.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RankingAScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      drawer: CustomDrawer(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          HeaderSimplesTile(),
          Expanded(
            child: Container(
              child: Consumer<RankingAManager>(
                builder: (_, rankingAManager, __) {
                  return ListView.builder(
                    padding: EdgeInsets.only(top: 10),
                      shrinkWrap: true,
                      itemCount: rankingAManager.allPlayers.length,
                      itemBuilder: (_, index) {
                        return PlayerListTile(
                            rankingAManager.allPlayers[index]);
                      });
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
