import 'dart:ui';

import 'package:flutter/material.dart';

class HeaderBaseTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Column(
      children: <Widget>[
        Material(
          borderRadius: const BorderRadius.only(
            bottomLeft: Radius.circular(40),
            bottomRight: Radius.circular(40),
          ),
          elevation: 20,
          color: const Color.fromRGBO(238, 162, 25, 1),
          // ignore: sized_box_for_whitespace
          child: Container(
            height: size.height * 0.40,
            child: Stack(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    gradient: RadialGradient(
                      radius: 0.8,
                      colors: [
                        Color.fromRGBO(241, 148, 31, 1),
                        Color.fromRGBO(224, 111, 10, 1),
                      ],
                    ),
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(30),
                      bottomRight: Radius.circular(30),
                    ),
                  ),
                  child: Center(
                    child: Image.asset(
                      "assets/images/logo.png",
                      height: 650,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
