import 'package:appfinalbrsports/models/player.dart';
import 'package:appfinalbrsports/models/rankingA_manager.dart';
import 'package:appfinalbrsports/models/user_manager.dart';
import 'package:appfinalbrsports/screens/base/base_screen.dart';
import 'package:appfinalbrsports/screens/login/login_screen.dart';
import 'package:appfinalbrsports/screens/rankingA/rankingAPlayer_screen.dart';
import 'package:appfinalbrsports/screens/signup/sign_up_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => UserManager(), lazy: false),
        ChangeNotifierProvider(create: (_) => RankingAManager(), lazy: false),
      ],
      child: Consumer<UserManager>(
        builder: (_, userManager, __) {
          return MaterialApp(
            title: 'BR Sports Ranking ',
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              scaffoldBackgroundColor: Color.fromRGBO(255, 255, 255, 1),
              appBarTheme: const AppBarTheme(
                elevation: 0,
                color: Colors.orangeAccent,
              ),
              visualDensity: VisualDensity.adaptivePlatformDensity,
            ),
            initialRoute: userManager.isLoggedIn ? '/base' : '/login',
            onGenerateRoute: (setings) {
              switch (setings.name) {
                case '/login':
                  return MaterialPageRoute(builder: (_) => LoginScreen());
                case '/signup':
                  return MaterialPageRoute(builder: (_) => SignUpScreen());
                  case '/playera':
                  return MaterialPageRoute(builder: (_) => RankingAPlayerScreen(setings.arguments as Player));
                case '/base':
                default:
                  return MaterialPageRoute(builder: (_) => BaseScreen());
              }
            },
          );
        },
      ),
    );
  }
}
